﻿

  CREATE TABLE "PAGES" 
   (
	"page_name" VARCHAR(255), 
	"page_text1" VARCHAR(255), 
	"page_text2" VARCHAR(255),
	"page_author"   VARCHAR(255),
	"page_date"   DATETIME, 
	"page_Status" VARCHAR (255),
	"page_delStatus" INT
   );


Insert into PAGES (page_name, page_text1, page_text2, page_author, page_date,page_Status,page_delStatus) values ('Page1','Welcome to Page 1','This is page number 1. You can edit this page, delete this page oe go back to home button and add a new page.','Hashmina Singh','7-12-2018', 'P' , 0);
Insert into PAGES (page_name, page_text1, page_text2, page_author, page_date,page_Status,page_delStatus) values ('Page2','Welcome to Page 2','This is page number 2. You can edit this page, delete this page oe go back to home button and add a new page.','Charanjit Kaur','7-12-2018', 'P' , 0);
Insert into PAGES (page_name, page_text1, page_text2, page_author, page_date,page_Status,page_delStatus) values ('Page3','Welcome to Page 3','This is page number 3. You can edit this page, delete this page oe go back to home button and add a new page.','Amrik Singh','7-12-2018', 'P' , 0);




