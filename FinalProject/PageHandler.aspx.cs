﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FinalProjectBN01313911

{
    public partial class PageManager : System.Web.UI.Page
    {
        private string insertNewPageQuery = "INSERT INTO pages " +
            "(page_title,page_text1,page_text2, page_author,page_date,page_Status,page_delStatus) VALUES";
        private string selectAllPagesQuery = "SELECT * FROM pages WHERE page_delStatus=0";

        protected void Page_Load(object sender, EventArgs e)
        {
            page_query.SelectCommand = selectAllPagesQuery;
            page_list.DataSource = Pages_Manual_Bind(page_query);
            page_list.CssClass = "table table-striped";
            page_list.DataBind();


        }
        protected DataView Pages_Manual_Bind(SqlDataSource src)
        {
            DataTable mytbl;
            DataView myview;
            mytbl = ((DataView)src.Select(DataSourceSelectArguments.Empty)).Table;


            DataColumn editcol = new DataColumn();
            editcol.ColumnName = "Edit";
            mytbl.Columns.Add(editcol);

            DataColumn delcol = new DataColumn();
            delcol.ColumnName = "Delete";
            mytbl.Columns.Add(delcol);

            mytbl.Columns["page_title"].ColumnName = "Page Title";
            mytbl.Columns["page_id"].ColumnName = "ID";
            mytbl.Columns["page_author"].ColumnName = "Author Name";
            mytbl.Columns["page_date"].ColumnName = "Date Created ";
            mytbl.Columns["page_modified_date"].ColumnName = "DatevModified";
            mytbl.Columns["page_Status"].ColumnName = "Status";



            foreach (DataRow row in mytbl.Rows)
            {
                //adding links 
                row["Page Title"] =
                    "<a href=\"PageView.aspx?pageid="
                    + row["ID"]
                    + "\">"
                    + row["Page Title"]
                    + "</a>";

                if (row["Status"].ToString() == "P")
                {
                    row["Status"] = "<a href=\"Updatestatus.aspx?pageid=" + row["ID"] + "&status=U" + "\"><img src='Images/purple.png'></a>";
                }
                else
                {
                    row["Status"] = "<a href=\"Updatestatus.aspx?pageid=" + row["ID"] + "&status=P" + "\"><img src='Images/blue.png'></a>";
                }

                row["Edit"] = "<a href=\"Editpage.aspx?pageid=" + row["ID"] + "\"><span class='btn btn-success'>Edit</span></a>";
                row["Delete"] = "<a href=\"Deletepage.aspx?pageid=" + row["ID"] + "\"><span class='btn btn-danger'>Delete</span></a>";

            }

            mytbl.Columns.Remove("page_text1");
            mytbl.Columns.Remove("page_text2");
            mytbl.Columns.Remove("page_delStatus");

            myview = mytbl.DefaultView;

            return myview;
        }

        protected void AddPage(object sender, EventArgs e)
        {
            ShowPage newPage = new ShowPage();
            newPage.PageTitle = pageTitle.Text.ToString();
            newPage.PageText1 = pageText1.Text.ToString();
            newPage.PageText2 = pageText2.Text.ToString();
            newPage.PageAuthor = pageAuthor.Text.ToString();
            newPage.PageStatus = pageStatus.SelectedValue.ToString();
            insertNewPageQuery += "('" + newPage.PageTitle + "','" + newPage.PageText1 + "','" + newPage.PageText2 + "','" + newPage.PageAuthor + "','" + newPage.PageStatus + "',GETDATE(),GETDATE(),0)";
            page_query.InsertCommand = insertNewPageQuery;
            page_query.Insert();
        }
    }
}