﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditPage.aspx.cs" Inherits="FinalProjectBN01313911.EditPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <asp:SqlDataSource runat="server" id="edit_page" ConnectionString="<%$ ConnectionStrings:finalproject_sql_con %>">
    </asp:SqlDataSource>
    
    <h3 runat="server" id="page_name">Update Page </h3>
    <!--Reference: In-class Example-->
    <div class="inputrow">
        <label>Page Title:</label>
        <asp:textbox id="pagename" runat="server">
        </asp:textbox>
    </div>

    <div class="inputrow">
        <label>Page Paragraph 1:</label>
        <asp:textbox id="pagebody1" runat="server">
        </asp:textbox>
    </div>

    <div class="inputrow">
        <label>Page Paragraph 2:</label>
        <asp:textbox id="pagebody2" runat="server">
        </asp:textbox>
    </div>

    <div class="inputrow">
        <label>Author's Name:</label>
        <asp:textbox id="authorname" runat="server">
        </asp:textbox>
    </div>
     
     
    <div class="inputrow">
        <label>Page Status:</label>
        <asp:textbox id="Pagestatus" runat="server">
        </asp:textbox>
    </div>
     
    <asp:Button Text="Edit Page " runat="server" OnClick="Edit_Page"/>
    
    <div class="querybox" id="debug" runat="server">

    </div>
    
</asp:Content>

