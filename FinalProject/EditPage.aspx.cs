﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//Reference: In-Class Example 
namespace FinalProjectBN01313911
{
    public partial class EditPage : System.Web.UI.Page
    {
        public int pageid
        {
            get { return Convert.ToInt32(Request.QueryString["pageid"]); }
        }


        protected void Page_Load(object sender, EventArgs e)
        {

        }
        
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            //Set the values for the student
            DataRowView pagerow = getPageData(pageid);
            if (pagerow == null)
            {
                page_name.InnerHtml = "No Student Found.";
                return;
            }
            pagename.Text = pagerow["page_title"].ToString();
            pagebody1.Text = pagerow["page_text1"].ToString();
            pagebody2.Text = pagerow["page_text2"].ToString();
            authorname.Text = pagerow["page_author"].ToString();
            if (!Page.IsPostBack)
            {
                page_name.InnerHtml += pagename.Text ;

            }
            page_name.Text = pagerow["page_name"].ToString();

        }

        protected void Edit_Student(object sender, EventArgs e)
        {
            string pname = pagename.Text;
            string body1 = pagebody1.Text;
            string body2 = pagebody2.Text;
            string author = authorname.Text;

            string editquery = "UPDATE pages SET page_name='" + pname + "'," +
                "page_text1='" + body1 + "',page_text2'" + body2 + "'," +
                "page_author='" + author + "', " +
                "page_Status='P'"  + "' where studentid=" + pageid;
            debug.InnerHtml = editquery;
            edit_page.UpdateCommand = editquery;
            edit_page.Update();

        }
        protected DataRowView getPageData(int id)
        {
            string query = "SELECT * FROM pages WHERE pageid=" + pageid.ToString();
            page_select.SelectCommand = query;
            
            DataView studentview = (DataView)page_select.Select(DataSourceSelectArguments.Empty);

            if (pageview.ToTable().Rows.Count < 1)
            {
                return null;
            }
            DataRowView pagerowview = pageview[0]; 
            return pagerowview;

        }

    }
}