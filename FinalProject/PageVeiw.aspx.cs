﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FinalProjectBN01313911
{
    public partial class Page : System.Web.UI.Page
    {
        public int pageid
        {
            get { return Convert.ToInt32(Request.QueryString["pageid"]); }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            base.OnPreRender(e);
            DataRowView pagerow = getPageInfo1(pageid);
            if (pagerow == null)
            {
                pagename.InnerHtml = "No Page Found.";
                return;
            }
            pagename.InnerHtml = pagerow["page_name"].ToString();
            body1.InnerHtml = pagerow["page_text1"].ToString();
            body2.InnerHtml = pagerow["page_text22"].ToString();
            authorname.InnerHtml = pagerow["page_author"].ToString();
            datecreated.InnerHtml = pagerow["page_date"].ToString();

        }
        protected override void OnPreRender(EventArgs e)
        {
            //base.OnPreRender(e);
           
        }
        protected DataRowView getPageInfo1(int id)
        {
            string query = "SELECT * FROM pages WHERE page_id=" + pageid.ToString();
            page_query1.SelectCommand = query;
            DataView pageview = (DataView)page_query1.Select(DataSourceSelectArguments.Empty);

            if (pageview.ToTable().Rows.Count < 1)
            {
                return null;
            }
            DataRowView pagerowview = pageview[0];
            return pagerowview;
        }
    }
}