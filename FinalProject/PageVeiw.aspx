﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PageVeiw.aspx.cs" Inherits="FinalProjectBN01313911.PageView" %>


<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
     <asp:SqlDataSource runat="server" id="page_query1" ConnectionString="<%$ ConnectionStrings:finalproject_sql_con %>">
    </asp:SqlDataSource>
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <h1 runat="server" ID="pagename"></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <em>Author Name:<span runat="server" ID="authorname"></span></em>
        </div>
        <div class="col-lg-6 col-md-6">
            <em>Page Created On:<span runat="server" ID="datecreated"></span></em>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <hr/>
        </div>
    </div>
    
    <div class="row">
        <div class="col-lg-6 col-md-6" runat="server" ID="body1">
        
        </div>
        <div class="col-lg-6 col-md-6" runat="server" ID="body2">
        
        </div>
    </div>
   
</asp:Content>


