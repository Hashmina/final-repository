﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PageHandler.aspx.cs" Inherits="FinalProjectBN01313911.PageHandler" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    
    <asp:SqlDataSource runat="server" id="page_query" ConnectionString="<%$ ConnectionStrings:finalproject_sql_con %>">
    </asp:SqlDataSource>

    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createNewPageModal">
        Create a New Page
    </button>
    <div class="modal fade" id="createNewPageModal" tabindex="-1" role="dialog" aria-labelledby="createNewPageModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="createNewPageModalLabel">Create a New Page Below:</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="CreatePage" runat="server">
                    <asp:Label runat="server">Page Title:</asp:Label>
                    <asp:TextBox runat="server" CssClass="form-control" ID="pageTitle" placeholder="Page Title"></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" ErrorMessage="Please Enter Title of the Page" ControlToValidate="pageTitle" ID="validatorPageTitle"></asp:RequiredFieldValidator>
                    <br/>
                    
                     <asp:Label runat="server">Page Paragraph 1:</asp:Label>
                    <asp:TextBox runat="server" CssClass="form-control" ID="pageText1" TextMode="multiline" Columns="50" Rows="5" placeholder="Page Paragraph-1"></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" ErrorMessage="Please Enter 1st Paragraph of Your Page" ControlToValidate="pageContent1" ID="validatorPageContent1"></asp:RequiredFieldValidator>
                    <br/>
                     <asp:Label runat="server">Page Paragraph 2:</asp:Label>
                    <asp:TextBox runat="server" CssClass="form-control" ID="pagText2" TextMode="multiline" Columns="50" Rows="5" placeholder="Page Paragraph-2"></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" ErrorMessage="Please Enter 2nd Paragraph of Your Page" ControlToValidate="pageContent2" ID="validatorPageContent2"></asp:RequiredFieldValidator>
                    <br />
                    <asp:Label runat="server">Author's Name:</asp:Label>
                    <asp:TextBox runat="server" CssClass="form-control" ID="pageAuthor" placeholder="Page Author"></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" ErrorMessage="Please Enter Name of The Author" ControlToValidate="pageAuthor" ID="validatorPageAuthor"></asp:RequiredFieldValidator>
                    <br/>
                     <asp:Label runat="server">Page Status:</asp:Label>
                    <asp:DropDownList runat="server" CssClass="form-control" ID="publishStatus">
                        <asp:ListItem Value="P" Text="Published"></asp:ListItem>
                        <asp:ListItem Value="U" Text="UnPublished"></asp:ListItem>
                    </asp:DropDownList>
                  </form>
                </div>
                <div class="modal-footer">
                      <asp:Button runat="server" CssClass="btn btn-secondary" ID="cancelButton" Text="Close" data-dismiss="modal"/>
                      <asp:Button runat="server" CssClass="btn btn-primary" ID="submitButton" OnClick="AddPage" Text="Submit"/>
                </div>
            </div>
        </div>
    </div>

    <asp:DataGrid ID="page_list" runat="server"></asp:DataGrid>
 

</asp:Content>