﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FinalProjectBN01313911
{
    public partial class SiteMaster : MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            DataView pages = getPageData();

            if (pages == null)
            {
                return;
            }
            foreach (DataRowView rowView in pages)
            {
                DataRow row = rowView.Row;
                 menu_bar.InnerHtml += "<li><a runat='server' href='/PageVeiw.aspx?pageid=" + row["page_id"] + "'>" + row["page_title"] + "</a></li>";
            }

        }
        protected DataView getPageData()
        {
            string query = "SELECT * FROM pages WHERE page_Status='P'";
            page_query.SelectCommand = query;

            DataView pageview = (DataView)page_query.Select(DataSourceSelectArguments.Empty);

            if (pageview.ToTable().Rows.Count < 1)
            {
                return null;
            }
            return pageview;

        }
    }
}